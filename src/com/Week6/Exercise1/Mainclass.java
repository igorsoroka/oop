package com.Week6.Exercise1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by User on 26.10.2015.
 */
public class Mainclass {
    public static void main(String[] args) {
        //Account acc = new Account();
        Scanner sc = new Scanner(System.in); //iniatilizing input from keyboard
        ArrayList<String> menu = new ArrayList<String>(); //for menu
        menu.add("1) Lis?? tavallinen tili");
        menu.add("2) Lis?? luotollinen tili");
        menu.add("3) Tallenna tilille rahaa");
        menu.add("4) Nosta tililt?");
        menu.add("5) Poista tili");
        menu.add("6) Tulosta tili");
        menu.add("7) Tulosta kaikki tilit");
        menu.add("0) Lopeta");
        ArrayList<Account> accounts = new ArrayList<>();
        while (true) { //cycle with user interface
            System.out.println();
            System.out.println("*** PANKKIJ?RJESTELM? ***");
            for (String x : menu) {
                System.out.println(x);
            }
            System.out.print("Valintasi: ");
            int choice = sc.nextInt();

            if (choice == 0) //stop
                break;
            if (choice == 1) { //add deposit account
                System.out.print("Sy?t? tilinumero: ");
                String choice2 = sc.next();
                DepositAccount depositAccount = new DepositAccount();
                depositAccount.setAccNum(choice2);

                System.out.print("Sy?t? raham??r?: ");
                int choice3 = sc.nextInt();
                depositAccount.setMoney(choice3);
                accounts.add(depositAccount); //adding to Accounts List for printing info next time

                System.out.println("Tilinumero: " + choice2);
                System.out.println("Raham??r?: " + choice3);
            }
            if (choice == 2) { //add credit account
                System.out.print("Sy?t? tilinumero: ");
                String choice2 = sc.next();
                CreditAccount creditAccount = new CreditAccount();
                creditAccount.setAccNum(choice2);

                System.out.print("Sy?t? raham??r?: ");
                int choice3 = sc.nextInt();
                creditAccount.setMoney(choice3);

                System.out.print("Sy?t? luottoraja: ");
                int choice4 = sc.nextInt();
                creditAccount.setCredit(choice4);
                accounts.add(creditAccount);

                System.out.println("Tilinumero: " + choice2);
                System.out.println("Raham??r?: " + choice3);
                System.out.println("Luotto: " + choice4);
            }
            if (choice == 3) {//put money into account
                System.out.print("Sy?t? tilinumero: ");
                String choice2 = sc.next();
                System.out.print("Sy?t? raham??r?: ");
                int choice3 = sc.nextInt();
                for (Account x : accounts) {
                    if (x.getAccNum().contains(choice2)) {
                        x.putMoney(choice3);
                    }
                }
                System.out.println("Tilinumero: " + choice2);
                System.out.println("Raham??r?: " + choice3);

            }
            if (choice == 4) {//withdraw money from account
                System.out.print("Sy?t? tilinumero: ");
                String choice2 = sc.next();
                System.out.print("Sy?t? raham??r?: ");
                int choice3 = sc.nextInt();
                for (Account x : accounts) {
                    if (x.getAccNum().contains(choice2)) {
                        x.withdrawMoney(choice3);
                    }
                }
                System.out.println("Tilinumero: " + choice2);
                System.out.println("Raham??r?: " + choice3);

            }
            if (choice == 5) {//remove account
                System.out.print("Sy?t? poistettava tilinumero: ");
                String choice2 = sc.next();
                for (Account x : accounts) {
                    if (x.getAccNum().contains(choice2))
                        accounts.remove(x);
                }
                accounts.trimToSize();
                System.out.println("Tilinumero: " + choice2);
            }
            if (choice == 6) {//print specific account
                System.out.print("Sy?t? tulostettava tilinumero: ");
                String choice2 = sc.next();
                for (Account x : accounts) {
                    if (x.getAccNum().equals(choice2)) {
                        System.out.println("Tilinumero: " + x.getAccNum());
                    }
                }
            }

            if (choice == 7) { //print all accounts

                System.out.println("Tulostaa kaiken");
                //for (Account x : accounts) {
                //   System.out.println(x.getAccNum());
                //}
            }
            if (choice > 7 || choice < 0)
                System.out.println("Valinta ei kelpaa.");
        }
    }
}




class Account {
    int money;
    String AccNum;

    public Account() {
        this.money = 0;
        this.AccNum = null;
    }

    public String getAccNum() {
        return AccNum;
    }

    public void setAccNum(String accNum) {
        AccNum = accNum;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void putMoney(int money) {
        this.money += money;
    }

    public void withdrawMoney(int money) {
        this.money -= money;
        if (this.money < 0)
            System.out.println("Overdraft!" + this.getMoney());;
    }

    public void printAll(ArrayList<Account> accounts) {

    }
}

class DepositAccount extends Account {}

class CreditAccount extends Account {
    int credit;

    public double getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    @Override
    public void withdrawMoney(int money) {
        this.money -= money;
        if (Math.abs(money) < this.credit) {
            int under = Math.abs(money) - this.credit;
            System.out.println("Under credit: " + under);
        }
    }
}