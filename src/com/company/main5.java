package com.company;

/**
 * Created by User on 18.09.2015.
 */
import java.util.Scanner;
public class main5 {
        public static void main (String[] args)
        {
            System.out.print("Anna koiralle nimi: ");
            Scanner sc = new Scanner(System.in);
            String name = sc.nextLine();
            Dog dog = new Dog(name);
            System.out.println(dog.builder());
            System.out.print("Mit? koira sanoo: ");
            String s = sc.nextLine();
            Dog dog1 = new Dog(name, s);
            dog1.speak();
        }
    }
    class Dog
    {
        private String name;
        private String saying;
        Dog (String name)
        {
            this.name = name;
        }
        Dog (String name, String saying)
        {
            this.name = name;
            this.saying = saying;
        }
        public String builder()
        {
            return "Hei, nimeni on " + name;
        }
        public void speak()
        {
            for (String x : saying.split(" "))
            {
                if (x.contains("true"))
                    System.out.println("Such boolean: " + x);
                else if (x.contains("false"))
                    System.out.println("Such boolean: " + x);
                else if (x.contains("0") || x.contains("1") || x.contains("2") || x.contains("3") || x.contains("4") || x.contains("5")
                        || x.contains("6") || x.contains("7") || x.contains("8") || x.contains("9"))
                    System.out.println("Such integer: " + x);
                else
                    System.out.println(x);
            }
        }
    }

