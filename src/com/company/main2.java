package com.company;

/**
 * Created by User on 17.09.2015.
 */
public class main2 {
    public static void main(String[] args) {
        // write your code here
        Dog rekku = new Dog("Rekku");
        Dog musti = new Dog("Musti");
        System.out.println(rekku.builder());
        System.out.println(musti.builder());
        System.out.println(rekku.speak());
        System.out.println(musti.speak());
    }
    public static class Dog
    {
        private String name;
        Dog (String name)
        {
            this.name = name;
        }
        public String builder()
        {
            return "Hei, nimeni on " + name + "!";
        }
        public String speak()
        {
            if (name.contains("Rekku"))
                return name + ": " + "Hau!";
            return name + ": " + "Vuh!";
        }
    }
}
