package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Dog rekku = new Dog("Rekku");
        Dog musti = new Dog("Musti");
        System.out.println(rekku.builder());
        System.out.println(musti.builder());
        System.out.println(rekku.speak());
    }
    public static class Dog
    {
        String name;
        Dog (String name)
        {
            this.name = name;
        }
        public String builder()
        {
            return "Hei, nimeni on " + name + "!";
        }
        public String speak()
        {
            return "Hau!\nVuh!";
        }
    }



}
