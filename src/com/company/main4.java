package com.company;

/**
 * Created by User on 18.09.2015.
 */
import java.util.Scanner;
public class main4 {
    public static void main (String[] args)
    {
        System.out.print("Anna koiralle nimi: ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        Dog dog = new Dog(name);
        System.out.println(dog.builder());
        System.out.print("Mit? koira sanoo: ");
        String s = sc.nextLine();
        Dog dog1 = new Dog(name, s);
        System.out.println(dog1.speak());
    }
    static class Dog
    {
        private String name;
        private String saying;
        Dog (String name)
        {
            this.name = name;
        }
        Dog (String name, String saying)
        {
            this.name = name;
            this.saying = saying;
        }
        public String builder()
        {
            if (name.trim().isEmpty()) {
                name = "Doge";
                return name;
            }
            return "Hei, nimeni on " + name + "!";
        }
        public String speak()
        {
            if (name.isEmpty())
                name = "Doge";
            System.out.println(name + ": " + "Much wow!");
            System.out.print("Mit? koira sanoo: ");
            Scanner sc = new Scanner(System.in);
            saying = sc.nextLine();
            while (saying.trim().isEmpty())
            {
                {
                    System.out.println(name + ": " + "Much wow!");
                    if (saying.trim().isEmpty())
                    {
                        System.out.print("Mit? koira sanoo: ");
                        saying = sc.nextLine();
                        break;
                    }
                }
            }
            saying = sc.nextLine();
            return name + ": " + saying;

        }
    }
}
