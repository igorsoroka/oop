package com.company;

/**
 * Created by User on 21.09.2015.
 */
public class week3ex3 {
    public static void main(String[] args) {
        BottleDispenser auto = new BottleDispenser();
        auto.addMoney();
        auto.buyBottle();
        auto.buyBottle();
        auto.addMoney();
        auto.addMoney();
        auto.buyBottle();
        auto.returnMoney();
    }
    static class BottleDispenser {
        private int bottles;
        private double money;

        public BottleDispenser() {
            bottles = 5;
            money = 0;
        }
        public void addMoney() {
            money += 1;
            System.out.println("Klink! Lis?? rahaa laitteeseen!");
        }
        public void buyBottle() {
            Bottle bottle = new Bottle (0, "0.5", 1.80);
            money -= bottle.getPrice();
            if (money < 0 || money == 0) {
                System.out.println("Sy?t? rahaa ensin!");
                money = 0;
            }
            else {
                bottles -= bottles;
                System.out.println("KACHUNK! " + bottle.getName() + " tipahti masiinasta!");
            }
        }
        public void returnMoney() {
            money = 0;
            System.out.println("Klink klink. Sinne meniv?t rahat!");
        }
    }
}
    class Bottle {
        private String name;
        String[] bottlesArray = new String [] {"Pepsi Max", "Pepsi", "0.3"};
        public String size;
        public double price;

        Bottle (int element) {
            this.name = bottlesArray[element];
        }
        Bottle (int element, String size, double price) {
            this.name = bottlesArray[element];
            this.size = size;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }
    }