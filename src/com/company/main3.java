package com.company;

/**
 * Created by User on 17.09.2015.
 */
import java.util.Scanner;
public class main3 {
    public static void main (String[] args) {
        System.out.print("Anna koiralle nimi: ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        Dog dog = new Dog(name);
        System.out.println(dog.builder());
        System.out.print("Mit? koira sanoo: ");
        String s = sc.nextLine();
        Dog dog1 = new Dog(name, s);
        System.out.println(dog1.speak());
    }
    public static class Dog
    {
        String name;
        String saying;
        Dog (String name)
        {
            this.name = name;
        }
        Dog (String name, String saying)
        {
            this.name = name;
            this.saying = saying;
        }
        public String builder()
        {
            return "Hei, nimeni on " + name + "!";
        }
        public String speak()
        {
            return name + ": " + saying;
        }
    }
}
