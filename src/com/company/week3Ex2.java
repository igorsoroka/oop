package com.company;

/**
 * Created by User on 21.09.2015.
 */
public class week3Ex2 {
    public static void main(String[] args) {
        BottleDispenser auto = new BottleDispenser();
        auto.addMoney();
        auto.buyBottle();
        auto.addMoney();
        auto.addMoney();
        auto.buyBottle();
        auto.returnMoney();
    }
    static class BottleDispenser {
            private int bottles;
            private int money;

            public BottleDispenser() {
                bottles = 5;
                money = 0;
            }
            public void addMoney() {
                money += 1;
                System.out.println("Klink! Lis?? rahaa laitteeseen!");
            }

            public void buyBottle() {
                bottles -= bottles;
                Bottle bottle = new Bottle(0);
                System.out.println("KACHUNK! " + bottle.getName() + " tipahti masiinasta!");
                money -= 1;
                if (money == 0)
                    System.out.println("Sy?t? rahaa ensin!");
            }
            public void returnMoney() {
                money = 0;
                System.out.println("Klink klink. Sinne meniv?t rahat!");
            }
        }
    }
    /*class Bottle {
        private String name;
        String[] bottlesArray = new String [] {"Pepsi Max", "Pepsi", "0.3"};

        Bottle (int element) {
            this.name = bottlesArray[element];
        }

        public String getName() {
            return name;
        }
    }*/
