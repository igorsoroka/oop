package com.week6Exercise3;

import java.util.ArrayList;

/**
 * Created by User on 04.11.2015.
 */
public abstract class Account {
    String AccNum;
    int money;
    //ArrayList<Account> accounts;
    public String getAccNum() {
        return AccNum;
    }
    public void setAccNum(String accNum) {
        AccNum = accNum;
    }
    public int getMoney() {
        return money;
    }
    public void setMoney(int money) {
        this.money = money;
    }
    public void putMoney(int money) {
        this.money += money;
    }
    public void withdrawMoney(int money) {
        this.money -= money;
        if (this.money < 0)
            System.out.println("Overdraft!" + this.getMoney());;
    }

}
