package com.week6Exercise3;

import java.util.ArrayList;

/**
 * Created by User on 04.11.2015.
 */
public class Bank {
    ArrayList<Account> accounts;
    public Bank(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }
    public class DepositAccount extends Account {}

    public class CreditAccount extends Account {
        int credit;

        public double getCredit() {
            return credit;
        }

        public void setCredit(int credit) {
            this.credit = credit;
        }

        @Override
        public void withdrawMoney(int money) {
            this.money -= money;
            if (Math.abs(money) < this.credit) {
                int under = Math.abs(money) - this.credit;
                System.out.println("Under credit: " + under);
            }
        }
    }
    public Account createDepositAccount() {
        return new DepositAccount();
    }
    public CreditAccount createCreditAccount() {
        return new CreditAccount();
    }
    public void printAll(ArrayList<Account> accounts) {}
}
