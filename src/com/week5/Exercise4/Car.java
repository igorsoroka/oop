package com.week5.Exercise4;


import java.util.ArrayList;

/**
 * Created by User on 15.10.2015.
 */
public class Car {
    Body body;
    Chassis chassis;
    Engine engine;
    Wheel wheel;
    Car () {
        this.body = new Body();
        this.chassis = new Chassis();
        this.engine = new Engine();
        this.wheel = new Wheel(4);
    }

    void print() {
        System.out.println("Autoon kuuluu:");
        System.out.println("\t" + Body.class.getSimpleName());
        System.out.println("\t" + Chassis.class.getSimpleName());
        System.out.println("\t" + Engine.class.getSimpleName());
        System.out.println("\t" + Car.this.wheel.num + " " + Wheel.class.getSimpleName());
    }

}
class Body {
    String body;
    Body() {
        this.body = "Body";
        System.out.println("Valmistetaan: " + this.body);
    }
}
class Chassis {
    String chassis;
    Chassis() {
        this.chassis = "Chassis";
        System.out.println("Valmistetaan: " + this.chassis);
    }
}
class Engine {
    String engine;
    Engine() {
        this.engine = "Engine";
        System.out.println("Valmistetaan: " + this.engine);
    }
}
class Wheel {
    private String w;
    public int num;
    Wheel(int num) {
        this.w = "Wheel";
        this.num = num;
        String[] wheels = new String[this.num];
        for (int i = 0; i < wheels.length; i++) {
            wheels[i] = this.w;
            System.out.println("Valmistetaan: " + this.w);
        }
    }
}

