package com.week4;
import java.io.*;

/**
 * Created by User on 25.09.2015.
 */
public class week4ex1 {
    public static void main(String[] args) {
        ReadAndWriteIO readWriteFile = new ReadAndWriteIO();
        String fileName = "C:\\Users\\User\\Desktop\\verse.txt";
        System.out.println( System.getProperty("user.dir") );
        readWriteFile.readFile(fileName);
    }
    static class ReadAndWriteIO {
        public String fileName;
        public String line = null;

        void readFile(String fileName) {
            try {
                FileReader fileReader = new FileReader(fileName);
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                while((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                }
                bufferedReader.close();
            }
            catch(FileNotFoundException ex) {
                System.out.println(
                        "Unable to open file '" +  fileName + "'");
            }
            catch(IOException ex) {
                System.out.println("Error reading file '"   + fileName + "'");
                // Or we could just do this:
                // ex.printStackTrace();
                fileName.endsWith(".txt");
            }
        }
    }
}
