package com.week4.week4ex2;

import java.io.*;
import java.util.zip.ZipFile;

/**
 * Created by User on 25.09.2015.
 */
public class week4ex2 {
    public static void main(String[] args) throws IOException {
        ReadAndWriteIO readWriteFile = new ReadAndWriteIO();
        String fileName = "C:\\Users\\User\\Desktop\\verse.txt";
        String fileNameOut = "C:\\Users\\User\\Desktop\\output.txt";
        readWriteFile.readWriteFile(fileName, fileNameOut);
        ZipFile zip = new ZipFile("C:\\Users\\User\\Desktop\\Verse.txt");
    }
    static class ReadAndWriteIO {
       public String line = null;

        void readWriteFile(String fileName, String fileNameOut) {
            try {
                FileReader fileReader = new FileReader(fileName);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                PrintWriter printLine = new PrintWriter(new FileWriter(fileNameOut, false));
                while ((line = bufferedReader.readLine()) != null) {
                    if ( line.length() < 30) {
                        if (!line.isEmpty() && line.charAt(0) != ' ' ) {
                            printLine.printf("%s" + "%n", line);
                        }
                    }
                }
                printLine.close();
                bufferedReader.close();
            } catch (FileNotFoundException ex) {
                System.out.println(
                        "Unable to open file '" +
                                fileName + "'");
            } catch (IOException ex) {
                System.out.println(
                        "Error reading file '"
                                + fileName + "'");
                // Or we could just do this:
                // ex.printStackTrace();
            }
        }
    }
}
