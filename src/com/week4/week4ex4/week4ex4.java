package com.week4.week4ex4;

import java.io.*;

/**
 * Created by User on 25.09.2015.
 */
public class week4ex4 {
    public static void main(String[] args) {
        ReadAndWriteIO readWriteFile = new ReadAndWriteIO();
        String fileName = "C:\\Users\\User\\Desktop\\verse.txt";
        String fileNameOut = "C:\\Users\\User\\Desktop\\output.txt";
        readWriteFile.readWriteFile(fileName, fileNameOut);
    }
    static class ReadAndWriteIO {
        public String line = null;

        void readWriteFile(String fileName, String fileNameOut) {
            try {
                FileReader fileReader = new FileReader(fileName);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                PrintWriter printLine = new PrintWriter(new FileWriter(fileNameOut, false));
                while ((line = bufferedReader.readLine()) != null) {
                    if ( line.length() < 30) {
                        if (!line.isEmpty() && line.charAt(0) != ' ' ) {
                            if(line.contains("v"))
                                printLine.printf("%s" + "%n", line);
                        }
                    }
                }
                printLine.close();
                bufferedReader.close();
            } catch (FileNotFoundException ex) {
                System.out.println(
                        "Unable to open file '" +
                                fileName + "'");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
