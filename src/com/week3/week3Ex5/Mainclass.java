package com.week3.week3Ex5;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by User on 22.09.2015.
 */
public class Mainclass {
    public static void main(String[] args) {
        BottleDispenser auto = new BottleDispenser();
        auto.CreateBottleList();
        ArrayList<String> menu = new ArrayList<String>();
        Scanner sc = new Scanner(System.in);
        menu.add("*** LIMSA-AUTOMAATTI ***");
        menu.add("1) Lis?? rahaa koneeseen");
        menu.add("2) Osta pullo");
        menu.add("3) Ota rahat ulos");
        menu.add("4) Listaa koneessa olevat pullot");
        menu.add("0) Lopeta");
        while(true) {
            System.out.println();
            for (String x : menu)
                System.out.println(x);
            System.out.print("Valintasi: ");
            int choice = sc.nextInt();
            if (choice == 1)
                auto.addMoney();
            if (choice == 2) {
                auto.bottleList();
                System.out.print("Valintasi: ");
                int choice2 = sc.nextInt() - 1;
                System.out.println(auto.buyBottle(choice2));
                auto.bottlesArray.remove(choice2);
                auto.bottlesArray.trimToSize();
            }
            if (choice == 3)
                auto.returnMoney();
            if (choice == 4)
                auto.bottleList();
            if (choice == 0)
                break;
        }
    }
    static class BottleDispenser {
        public int bottles;
        public double money;
        ArrayList<Bottle> bottlesArray = new ArrayList<Bottle>();
        public void CreateBottleList() {
            for (int i = 0; i < this.bottles; i++){
                bottlesArray.add(i, new Bottle(i,i,i));
            }
        }

        public BottleDispenser() {
            bottles = 6;
            money = 0;
        }
        public void addMoney() {
            money = money + 1;
            System.out.println("Klink! Lis?? rahaa laitteeseen!");
        }
        public String buyBottle(int num) {
            if (money <= 0) {
                money = 0;
                return "Sy?t? rahaa ensin!";
            }
            else if (money < bottlesArray.get(num).getPrice())
                return "Sy?t? rahaa ensin!";
            else {
                money = money - bottlesArray.get(num).getPrice();
                bottles = bottles - 1;

                return "KACHUNK! " + bottlesArray.get(num).getName() + " tipahti masiinasta!";
            }
        }
        public void returnMoney() {
            System.out.print("Klink klink. Sinne meniv?t rahat! Rahaa tuli ulos ");
            System.out.printf("%2.2f", money);
            System.out.println("\u20ac");
        }
        public void bottleList(){
            for (int i = 0; i < bottlesArray.size(); i++) {
                    System.out.println(i + 1 + ". Nimi: " + bottlesArray.get(i).getName());
                    System.out.println("\tKoko: " + bottlesArray.get(i).getSize() + "\tHinta: " + bottlesArray.get(i).getPrice());
                }
            }
        }
    }
class Bottle {
    private String name, size;
    private double price;
    public String[] manArray = new String [] {"Pepsi Max", "Pepsi Max", "Coca-Cola Zero", "Coca-Cola Zero",
            "Fanta Zero", "Fanta Zero"};
    public String[] sizeArray = new String[]{"0.5", "1.5", "0.5", "1.5", "0.5", "0.5"};
    public double[] priceArray = new double[]{1.8, 2.2, 2.0, 2.5, 1.95, 1.95};

    Bottle (int manElement, int sizeElement, int priceElement) {
        this.name = manArray[manElement];
        this.size = sizeArray[sizeElement];
        this.price = priceArray[priceElement];
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getSize() {
        return size;
    }
}
