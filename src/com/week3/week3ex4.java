package com.week3;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by User on 21.09.2015.
 */
public class week3ex4 {
    public static void main(String[] args) {
        BottleDispenser auto = new BottleDispenser();
        ArrayList<String> menu = new ArrayList<String>();
        Scanner sc = new Scanner(System.in);
        menu.add("*** LIMSA-AUTOMAATTI ***");
        menu.add("1) Lis?? rahaa koneeseen");
        menu.add("2) Osta pullo");
        menu.add("3) Ota rahat ulos");
        menu.add("4) Listaa koneessa olevat pullot");
        menu.add("0) Lopeta");
        while(true) {
            System.out.println();
            for (String x : menu)
                System.out.println(x);
            System.out.print("Valintasi: ");
            int choice = sc.nextInt();
            if (choice == 1)
                auto.addMoney();
            if (choice == 2)
                auto.buyBottle();
            if (choice == 3)
                auto.returnMoney();
            if (choice == 4)
                auto.bottleList();
            if (choice == 0)
                break;
        }
    }
    static class BottleDispenser {
        public int bottles;
        public double money;
        Bottle bottle = new Bottle (0, "0.5", 1.80);

        public BottleDispenser() {
            bottles = 6;
            money = 0;
        }
        public void addMoney() {
            money = money + 1;
            System.out.println("Klink! Lis?? rahaa laitteeseen!");
        }
        public void buyBottle() {
            if (money <= 0) {
                money = 0;
                System.out.println("Sy?t? rahaa ensin!");
            }
            else if (money < bottle.getPrice())
                System.out.println("Sy?t? rahaa ensin!");
            else {
                money = money - bottle.getPrice();
                bottles = bottles - 1;
                System.out.println("KACHUNK! " + bottle.getName() + " tipahti masiinasta!");
            }
        }
        public void returnMoney() {
            money = 0;
            System.out.println("Klink klink. Sinne meniv?t rahat!");
        }
        public void bottleList(){
             for (int i = 0; i < this.bottles; i++) {
                System.out.println(i+1 + ". Nimi: " + bottle.getName());
                System.out.println("\tKoko: " + bottle.getSize() + "\tHinta: " + bottle.getPrice());
            }
        }
    }
}
class Bottle {
    private String name;
    String[] bottlesArray = new String [] {"Pepsi Max", "Pepsi", "0.3"};
    public String size;
    public double price;

    Bottle (int element) {
        this.name = bottlesArray[element];
    }
    Bottle (int element, String size, double price) {
        this.name = bottlesArray[element];
        this.size = size;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getSize() {
        return size;
    }
}
