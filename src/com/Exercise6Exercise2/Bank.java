package com.Exercise6Exercise2;

import java.util.ArrayList;

/**
 * Created by User on 30.10.2015.
 */
public class Bank {
    ArrayList<Account> accounts;
    public Bank(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }
    public class Account {
        int money;
        String AccNum;

        public Account() {
            this.money = 0;
            this.AccNum = null;
        }

        public String getAccNum() {
            return AccNum;
        }

        public void setAccNum(String accNum) {
            AccNum = accNum;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(int money) {
            this.money = money;
        }

        public void putMoney(int money) {
            this.money += money;
        }

        public void withdrawMoney(int money) {
            this.money -= money;
            if (this.money < 0)
                System.out.println("Overdraft!" + this.getMoney());;
        }

        public void printAll(ArrayList<Account> accounts) {

        }
    }

    public class DepositAccount extends Account {}

    public class CreditAccount extends Account {
        int credit;

        public double getCredit() {
            return credit;
        }

        public void setCredit(int credit) {
            this.credit = credit;
        }

        @Override
        public void withdrawMoney(int money) {
            this.money -= money;
            if (Math.abs(money) < this.credit) {
                int under = Math.abs(money) - this.credit;
                System.out.println("Under credit: " + under);
            }
        }
    }
    public Account createDepositAccount() {
        return new DepositAccount();
    }
    public CreditAccount createCreditAccount() {
        return new CreditAccount();
    }
}
