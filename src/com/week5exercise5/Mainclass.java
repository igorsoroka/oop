package com.week5exercise5;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by User on 21.10.2015.
 */
public class Mainclass {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> menu = new ArrayList<>(); //main menu
        menu.add("1) Luo hahmo");
        menu.add("2) Taistele hahmolla");
        menu.add("0) Lopeta");

        ArrayList<String> subMenuChar = new ArrayList<>(); // submenu of characters
        subMenuChar.add("1) Kuningas");
        subMenuChar.add("2) Ritari");
        subMenuChar.add("3) Kuningatar");
        subMenuChar.add("4) Peikko");

        ArrayList<String> subMenuWeapons = new ArrayList<>(); //submenu of weapons
        subMenuWeapons.add("1) Veitsi");
        subMenuWeapons.add("2) Kirves");
        subMenuWeapons.add("3) Miekka");
        subMenuWeapons.add("4) Nuija");

        int choice3 = 0;
        Object choosedChar = null;
        String choosedWeapon = null;
        while(true) {
            System.out.println("*** TAISTELUSIMULAATTORI ***");
            for (String j : menu) {
                System.out.println(j);
            }
            System.out.print("Valintasi: ");
            int choice = sc.nextInt();
            if (choice == 1) {
                System.out.println("Valitse hahmosi:");
                for (String f : subMenuChar) {
                    System.out.println(f);
                }
                System.out.print("Valintasi: ");
                int choice2 = sc.nextInt();
                if (choice2 == 1) {choosedChar = new King();}
                if (choice2 == 2) {choosedChar = new Knight();}
                if (choice2 == 3) {choosedChar = new Queen();}
                if (choice2 == 4) {choosedChar = new Troll();}
                System.out.println("Valitse aseesi: ");
                for (String g : subMenuWeapons){
                    System.out.println(g);
                }
                System.out.print("Valintasi: ");
                choice3 = sc.nextInt();
                if (choice3 == 1) {choosedWeapon = "Knife";}
                if (choice3 == 2) {choosedWeapon = "Axe";}
                if (choice3 == 3) {choosedWeapon = "Sword";}
                if (choice3 == 4) {choosedWeapon = "Club";}
            }
            if (choice == 2) {
                if (choosedChar instanceof King)
                    ((King) choosedChar).fight(choosedWeapon);
                if (choosedChar instanceof Knight)
                    ((Knight) choosedChar).fight(choosedWeapon);
                if (choosedChar instanceof Queen)
                    ((Queen) choosedChar).fight(choosedWeapon);
                if (choosedChar instanceof Troll)
                    ((Troll) choosedChar).fight(choosedWeapon);
            }
            if (choice == 0) {
                break;
            }
        }
    }
}
