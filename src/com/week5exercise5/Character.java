package com.week5exercise5;

/**
 * Created by User on 21.10.2015.
 */
public class Character {
    public WeaponBehavior weapon;
    public Character() {}
    public void fight(String gun) {
        if (gun.contains("Club"))
            this.weapon = new ClubBehavior();
        if (gun.contains("Sword"))
            this.weapon = new SwordBehavior();
        if (gun.contains("Axe"))
            this.weapon = new AxeBehavior();
        if (gun.contains("Knife"))
            this.weapon = new KnifeBehavior();
        System.out.println(this.getClass().getSimpleName() + " tappelee aseella " + this.weapon.useWeapon());
    }
}
class WeaponBehavior {
    String useWeapon() {
        return getClass().getSimpleName().replace("Behavior", "");
    }
}
class ClubBehavior extends WeaponBehavior {    }
class SwordBehavior extends WeaponBehavior  {}
class AxeBehavior extends WeaponBehavior {}
class KnifeBehavior extends WeaponBehavior {}
class Troll extends Character {}
class Knight extends Character {}
class King extends Character {}
class Queen extends Character {

}
