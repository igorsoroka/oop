package com.Week6Exercise4;

/**
 * Created by User on 04.11.2015.
 */
public abstract class Account {
    String AccNum;
    int money;
    public String getAccNum() {
        return AccNum;
    }
    public void setAccNum(String accNum) {
        AccNum = accNum;
    }
    public int getMoney() {
        return money;
    }
    public void setMoney(int money) {
        this.money = money;
    }
    public void putMoney(int money) {
        this.money += money;
    }
    public void withdrawMoney(int money) {
        this.money -= money;
        if (this.money < 0)
            System.out.println("Overdraft!" + this.getMoney());;
    }
}
class DepositAccount extends Account {}
class CreditAccount extends Account {
    int credit;
    public int getCredit() {
        return credit;
    }
    public void setCredit(int credit) {
        this.credit = credit;
    }
    @Override
    public void withdrawMoney(int money) {
        this.money -= money;
        if (Math.abs(money) < this.credit) {
            int under = Math.abs(money) - this.credit;
            System.out.println("Under credit: " + under);
        }
    }
}
