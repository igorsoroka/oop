package com.Week6Exercise4;

/**
 * Created by User on 04.11.2015.
 */
import java.util.ArrayList;
import java.util.Scanner;

public class Mainclass {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); //iniatilizing input from keyboard
        ArrayList<String> menu = new ArrayList<String>(); //for menu
        menu.add("1) Lis?? tavallinen tili");
        menu.add("2) Lis?? luotollinen tili");
        menu.add("3) Tallenna tilille rahaa");
        menu.add("4) Nosta tililt?");
        menu.add("5) Poista tili");
        menu.add("6) Tulosta tili");
        menu.add("7) Tulosta kaikki tilit");
        menu.add("0) Lopeta");
        ArrayList<Account> accounts = new ArrayList<Account>();
        Bank bank = new Bank(accounts);
        while (true) { //cycle with user interface
            System.out.println();
            System.out.println("*** PANKKIJ?RJESTELM? ***");
            for (String x : menu) {
                System.out.println(x);
            }
            System.out.print("Valintasi: ");
            int choice = sc.nextInt();

            if (choice == 0) //stop
                break;
            if (choice == 1) { //add deposit account
                System.out.print("Sy?t? tilinumero: ");
                String choice2 = sc.next();
                Account depositAccount = bank.createDepositAccount();
                depositAccount.setAccNum(choice2);

                System.out.print("Sy?t? raham??r?: ");
                int choice3 = sc.nextInt();
                depositAccount.setMoney(choice3);
                accounts.add(depositAccount); //adding to Accounts List for printing info next time

                System.out.println("Tili luotu.");
            }
            if (choice == 2) {                          //add credit account
                System.out.print("Sy?t? tilinumero: ");
                String choice2 = sc.next();
                CreditAccount creditAccount = bank.createCreditAccount();
                creditAccount.setAccNum(choice2);

                System.out.print("Sy?t? raham??r?: ");
                int choice3 = sc.nextInt();
                creditAccount.setMoney(choice3);

                System.out.print("Sy?t? luottoraja: ");
                int choice4 = sc.nextInt();
                creditAccount.setCredit(choice4);
                accounts.add(creditAccount);

                System.out.println("Tili luotu.");
            }
            if (choice == 3) {                              //put money into account
                System.out.print("Sy?t? tilinumero: ");
                String choice2 = sc.next();
                System.out.print("Sy?t? raham??r?: ");
                int choice3 = sc.nextInt();
                for (Account x : accounts) {
                    if (x.getAccNum().contains(choice2)) {
                        x.putMoney(choice3);
                    }
                }
            }
            if (choice == 4) {//withdraw money from account
                System.out.print("Sy?t? tilinumero: ");
                String choice2 = sc.next();
                System.out.print("Sy?t? raham??r?: ");
                int choice3 = sc.nextInt();
                for (Account x : accounts) {
                    if (x.getAccNum().contains(choice2)) {
                        x.withdrawMoney(choice3);
                    }
                }
            }
            if (choice == 5) {//remove account
                System.out.print("Sy?t? poistettava tilinumero: ");
                String choice2 = sc.next();
                int position = 0;
                boolean location = false;
                for (Account x : accounts) {
                    if (x.getAccNum().contains(choice2)) {
                        position = accounts.indexOf(x);
                        location = true;
                    }
                }
                if (location)
                    accounts.remove(position);
                accounts.trimToSize();
                System.out.println("Tili poistettu.");
            }
            if (choice == 6) {//print specific account
                System.out.print("Sy?t? tulostettava tilinumero: ");
                String choice2 = sc.next();
                int accMoney = 0;
                for (Account x : accounts) {
                    if (x.getAccNum().contains(choice2)) {
                        accMoney = x.getMoney();
                    }
                }
                System.out.println("Tilinumero: " + choice2 +  " Tilill? rahaa: " + accMoney);
            }

            if (choice == 7) { //print all accounts
                System.out.println("Kaikki tilit:");
                for (Account x : accounts) {
                    if (x instanceof DepositAccount)
                        System.out.println("Tilinumero: " + x.getAccNum() + " Tilill? rahaa: " + x.getMoney());
                    if (x instanceof CreditAccount)
                        System.out.println("Tilinumero: " + x.getAccNum() + " Tilill? rahaa: " + x.getMoney() + " Luottoraja: " + ((CreditAccount) x).getCredit());
                }
            }
            if (choice > 7 || choice < 0)
                System.out.println("Valinta ei kelpaa.");
        }
    }
}