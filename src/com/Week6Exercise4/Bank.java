package com.Week6Exercise4;

/**
 * Created by User on 04.11.2015.
 */
import java.util.ArrayList;

public class Bank {
    ArrayList<Account> accounts;
    public Bank(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }
    public Account createDepositAccount() {
        return new DepositAccount();
    }
    public CreditAccount createCreditAccount() {
        return new CreditAccount();
    }
}
